<?php

/**
 * 361GRAD Element Ctateaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_ctateaser'] =
    '{type_legend},type;' .
    '{headline_legend},headline;' .
    '{text_legend},text;' .
    '{image_legend},addImage;' .
    '{link_legend},url,target,dse_titleText;' .
    '{link2_legend},dse_ctaHref,dse_ctaTitle,dse_ctaHrefTarget;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{custom_legend},dse_isScrollbutton,dse_scrollButtonId;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['floating']['options']       = ['left', 'right'];
$GLOBALS['TL_DCA']['tl_content']['fields']['url']['eval']['mandatory']  = false;
$GLOBALS['TL_DCA']['tl_content']['fields']['text']['eval']['mandatory'] = false;

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_linkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_titleText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_titleText'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHrefTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHrefTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isScrollbutton'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isScrollbutton'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_scrollButtonId'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_scrollButtonId'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
