<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_ctateaser'] = ['CTA Teaser', 'CTA Teaser with text and link (optional)'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Headline settings';

$GLOBALS['TL_LANG']['tl_content']['dse_titleText']  =
    ['Link Text', 'Here you can add Link Text.'];    

$GLOBALS['TL_LANG']['tl_content']['link2_legend']   = 'Hyperlink (second) settings';
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle']  = ['CTA Button Title', 'The link title will be displayed on hover.'];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHrefTarget']  =
    ['Target', 'Check this if you want open a link in new window.'];    

$GLOBALS['TL_LANG']['tl_content']['custom_legend']   = 'Custom settings';
$GLOBALS['TL_LANG']['tl_content']['dse_isScrollbutton']  =
    ['Scroll Down button', 'Is this a element has Scroll Down button?'];
$GLOBALS['TL_LANG']['tl_content']['dse_scrollButtonId']  =
    ['Scroll to ID', 'Use this field to add the ID to which the page will be scrolled' ];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];